import unittest
from main import CDSoftware


class Test(unittest.TestCase):
    testObject = CDSoftware("Jenkins", 2.18)  # creating instance of the class

    def test_name(self):  # testing the name of the object
        self.assertEqual("Jenkins", self.testObject.name)

    def test_version(self):  # testing the version
        self.assertTrue(2.161 <= self.testObject.version <= 2.187)


if __name__ == "__main__":
    unittest.main()
    print("Unit tests are successful, ready for deployment. ")