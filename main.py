""" A simple class to test Jenkins CI automation """


class CDSoftware:
    """ A simple class that will be used in this example """

    def __init__(self, name, version):
        self.name = name
        self.version = version

    def return_name(self):
        """Returning the name"""
        print(f"{self.name} is a tool for CI/CD")

    def version_check(self):
        """ Checking the version of the software"""
        print(f"The version of {self.name} is {self.version}")


JENKINS = CDSoftware("Jenkins", 2.18)
JENKINS.return_name()
JENKINS.version_check()
